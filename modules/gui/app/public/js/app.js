'use strict';

angular.module('fileExplorer', [])
    .factory('GUI', function () {
        return require('nw.gui');
    })
    .factory('Window', ['GUI', function (gui) {
        var window = gui.Window.get();
        window.focus();
        return window;
    }])
    .controller('Toolbar', ['$scope', 'Window',
        function ($scope, Window) {
            $scope.minimize = function () {
                Window.minimize();
            };

            $scope.toggleFullscreen = function () {
                Window.toggleKioskMode();
            };

            $scope.close = function () {
                Window.close();
            };
        }
    ]);
