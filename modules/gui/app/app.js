var express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  requireDir = require('require-dir');

var app = express();

app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));

app.set('port', process.env.PORT || 3000);

var routes = requireDir('./routes');
for (var i in routes) app.use('/', routes[i]);


var server = app.listen(app.get('port'), function() {
	console.log("Listening on port: " + app.get('port'));
});

module.exports = app;